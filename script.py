#!/usr/bin/env python

import sys
import os
from os import listdir
from os.path import isfile, join
import re
import readline

from math import sqrt

# To handle write/read objects from files
import pickle

# Seconds to minutes convertion
import datetime

# Find the address given the coordinates
# need to run 'pip install geopy' first
from geopy.geocoders import Nominatim

import numpy as np

def cart2pol(x, y):
    rho = np.sqrt(x**2 + y**2)
    phi = np.arctan2(y, x)
    return(rho, phi)
def pol2cart(rho, phi):
    x = rho * np.cos(phi)
    y = rho * np.sin(phi)
    return(x, y)

THRESHOLD = 0.01

class Track :
    """ The track object """
    def __init__(self):
        self.date = 'No Date'
        self.classification = 'No Classification'
        self.total_time = 0
        self.total_meters = 0
        self.maxlong = []
        self.maxlat = []
        self.minlong =[]
        self.minlat = []
    def __str__(self):
        return ','.join([
                str(self.date), 
                str(self.classification), 
                str(datetime.timedelta(seconds=self.total_time)), 
                str(self.total_meters/1000) 
               # str(self.address)
               ])


def extract_data( pth ):
    """ Open the file pth, 
    extract the data such as TotalTimeSeconds, DistanceMeters, Track (set of Coordinates, one approx every 10meters ) """

    tcx_file = open(pth, 'r')
    lst_lines = tcx_file.readlines()
    tcx_file.close()

    # Extract the data from tags <TotalTimeSeconds> <DistanceMeters> <Time>
    # When it finds the tag <Time> it stops looking for extra informations.
    # If it never finds the tag <Time> classifies the file as not so interesting, in this case the function returns None
    flag_date = 0
    trk = Track();
    date_str = None

    for line in lst_lines:
        if '<TotalTimeSeconds>' in line:
            trk.total_time = int(re.search('<TotalTimeSeconds>(.+?)</TotalTimeSeconds>', line).group(1))
        if '<DistanceMeters>' in line:
            trk.total_meters = float(re.search('<DistanceMeters>(.+?)</DistanceMeters>', line).group(1))
        if '<Time>' in line:
            date_str = re.search('<Time>(.+?)</Time>', line).group(1)
            break;
    # Check if the file is interesting
    if date_str == None :
        return None
    else :
        trk.date = date_str

    count = 0
    flag_point = 0
    trk.maxlong = None
    trk.minlong = None
    trk.maxlat = None
    trk.minlat = None
    

    # Find MAXLONG number of points where the long is maximum
    for line in lst_lines:
        if '<Track>' in line : 
                flag_point = 1
        if '<LatitudeDegrees>' in line and flag_point == 1 :
            lat = float(re.search('<LatitudeDegrees>(.+?)</LatitudeDegrees>', line).group(1))
        if '<LongitudeDegrees>' in line and flag_point == 1 :
            lon = float(re.search('<LongitudeDegrees>(.+?)</LongitudeDegrees>', line).group(1))
        if '</Track>' in line : 
            #MAXIMUM
            if trk.maxlong == None or trk.maxlong[1] < lon :
                trk.maxlong = (lat,lon)
            if trk.maxlat == None or trk.maxlat[0] < lat :
                trk.maxlat = (lat,lon)
            #MINIMUM
            if trk.minlong == None or trk.minlong[1] > lon :
                trk.minlong = (lat,lon)
            if trk.minlat == None or trk.minlat[0] > lat :
                trk.minlat = (lat,lon)
            flag_point = 0
    # # Compile the list of points lat,long
    # # It takes one coordinate every 30 points
   # for line in lst_lines:
   #     if '<Track>' in line : 
   #         if (count % 30 == 0)  :
   #             flag_point = 1
   #         else :
   #             count = count + 1
   #     if '<LatitudeDegrees>' in line and flag_point == 1 :
   #         lat = float(re.search('<LatitudeDegrees>(.+?)</LatitudeDegrees>', line).group(1))
   #     if '<LongitudeDegrees>' in line and flag_point == 1 :
   #         lon = float(re.search('<LongitudeDegrees>(.+?)</LongitudeDegrees>', line).group(1))
   #     if '</Track>' in line : 
   #         trk.list_points.append( (lat,lon) )
   #         flag_point = 0
    return trk


def distance( p1, p2 ):
    return sqrt((p1[0]-p2[0])**2 + (p1[1]-p2[1])**2)

#def similarity( a, b ):
#    # compute basic similarity, the smaller the value the better
#    delta_stpoint = float(distance( a.list_points[0] , b.list_points[0] ))/100
#    delta_total_hm = float(abs(a.total_meters - b.total_meters))/100
#    track_similarity = 0
#    # if the tracks are really different it doesnt make sense compute the similarity of the list of points
#    if (delta_stpoint < 2) and (delta_total_hm < 10):
#        # compute the similarity between two list of points, the smaller the value the better the similarity
#        # The similarity is computed as the cardinality (normalized) of the set of points that are close enough between the two,
#        track_similarity = float(sum([ 1 for p1 in a.list_points for p2 in b.list_points if distance(p1,p2)<10 ]))/len(a.list_points);
#        # Old track similarity below
#        #track_similarity = float(sum([ distance(a,b) for (a,b) in zip(a.list_points, b.list_points)]))/ (100*min(a.total_meters, b.total_meters))
#    return (delta_stpoint + delta_total_hm - track_similarity)
#


def similarity( a, b ):
    # compute basic similarity, the smaller the value the better
    alist = [ a.minlat, a.minlong, a.maxlat, a.maxlong ]
    blist = [ b.minlat, b.minlong, b.maxlat, b.maxlong ]
    return   sum([ distance( p1,p2 ) for (p1,p2) in zip(alist,blist)])


def prompt(new_track, filename ):
    if new_track == None :
        print(' ---- No interesting data has been found in the file, the file will be skipped\n\n')
    else :
        if new_track.date not in [ t.date for t in tracks ] :
            tracks.append(new_track)
        print(' ---- Extracted data : \n   '+str(new_track)+'\n')
    
        if len(tags) == 0 :
            print(' - It seems your database of the tags is empty!\n')
            print(' --- I am opening gpxsee for you\n')    
            os.system('gpxsee '+filename+' &> /dev/null &')
            # Ask to give a name
            print(' --- Give a good tag for this track:\n')    
            new_track.classification = raw_input()
            print(' - I am adding this new classification to the database\n')
            # I add the new_track in the database of the tags with hash value its fresh new classification
            tags[new_track] = new_track.classification
        else :
            # Compute the similarities scores
            matches = [ (tags[key], similarity(key,new_track)) for key in tags.keys() ]
            print(' - Here are the similarities score with the know tracks:\n'+ str(matches))
            min_key = min( matches, key= lambda (v,i) : i )
            if min_key[1] < THRESHOLD :
                print(' --- I am opening gpxsee for you\n')    
                os.system('gpxsee '+filename+' &> /dev/null &')
                print('I am classifying this as '+min_key[0]+'\n Is it Okay with you?\n')
                ans = raw_input()
                if ans == 'y' :
                    new_track.classification = min_key[0]
                else :
                    new_track.classification = ans
            else :
                print(' ---- Could not find a match, assign it manually:\n')
                print(' --- I am opening gpxsee for you\n')    
                os.system('gpxsee '+filename+' &> /dev/null &')
                new_track.classification = raw_input()
                tags[new_track] = new_track.classification





def convert():
    #Database of the files already processed FIT files
    db_files = open('./TMP/db','a+')
    lst_db = [ s[:-1] for s in db_files.readlines()]
    
    #Look for .FIT files in the path, skipping all the files already processed previously
    fit_names = [f for f in listdir(path) if isfile(join(path, f)) and f.endswith('.FIT') and f not in lst_db ]
    
    #Writing the new processed files in the database of the already processed FIT files, so to not have doubles and close the file
    db_files.writelines([ s+'\n' for s in fit_names])  
    db_files.close()
    
    for n in fit_names:
        print('*****************************\n')
        print(' - Converting  the file '+n+' in TCX format\n')
        os.system('gpsbabel -i garmin_fit -f '+path+'/'+n+' -o gtrnctr -F ./TMP/'+n+'.tcx')
        print(' - Extracting the data from the tcx file\n')
        new_track = extract_data( './TMP/'+n+'.tcx')
        prompt(new_track,'./TMP/'+n+'.tcx') 
        if (new_track != None) :
            print(' - Save the TCX file in: ./Data/'+new_track.date+'.tcx\n')
            os.system('cp ./TMP/'+n+'.tcx ./Data/'+new_track.date+'.tcx')
        os.system('rm ./TMP/'+n+'.tcx')





def fromtcx():
    #Look for .tcx files in the path
    tcx_names = [f for f in listdir(path) if isfile(join(path, f)) ]
    for n in tcx_names:
        print('*****************************\n')
        print(' - Extracting the data from the tcx file\n')
        new_track = extract_data( './Data/'+n )
        prompt(new_track,'./Data/'+n) 



##################################################################################
##################################################################################
##################################################################################
############# STARTING POINT OF THE SCRIPT #######################################
##################################################################################
##################################################################################
##################################################################################


#Open/Create the dictionary for the tags: the dictionary use as value a string (Madrid Rio, Casa de Campo, etx) and key a Track object
tags = None
try :
    db_tags = open('./TMP/db_tags','r+')
    tags = pickle.load(db_tags)
    db_tags.close()
except IOError:
    tags = { }

#Open/Create the list for the tracks: the list contains all the tracks 
tracks = None
try :
    db_tracks = open('./TMP/db_tracks','r+')
    tracks = pickle.load(db_tracks)
    db_tracks.close()
except IOError:
    tracks = []

# Checking the arguments, if not given it will abort, a bit dirty but works
command=sys.argv[1]
if command == 'convert' :
    path=sys.argv[2]
    if path.endswith('/') : path = path[:-1]
    convert()

if command == 'fromtcx' :
    path= './Data'
    fromtcx()


print(' - Here some yummy statistics:\n')
now = datetime.datetime.now()
month_str = str(now.year)+'-'+str(now.month)
km_month = sum([ t.total_meters/1000 for t in tracks if month_str in t.date ])
print(' --- This month you run '+str(km_month)+' km\n')


## Fastest run of the month
pace = lambda t : (float(t.total_time)/60)/(float(t.total_meters)/1000)
maxtrack = min([ t for t in tracks if month_str in t.date], key = pace )
# gets the sec format for the maxpace
maxpacemin = int(pace(maxtrack))
maxpacesec = int((pace(maxtrack) - maxpacemin) *60)
print(' --- The fastest track of this month was '+str(maxtrack)+'\n --- your avg pace was '+str(maxpacemin)+':'+str(maxpacesec)+'min/km\n')

## Last month amount of km
if now.month == 1:
    last_month_str = str(now.year-1)+'-'+str(12)
else :
    last_month_str = str(now.year)+'-'+str(now.month-1)
km_month = sum([ t.total_meters/1000 for t in tracks if last_month_str in t.date ])
print(' --- Last month you run '+str(km_month)+' km\n')


## All the year amount of km
km_year = sum([ t.total_meters/1000 for t in tracks if str(now.year) in t.date ])
print(' --- This year you have been running for '+str(km_year)+' km\n')

## Fastest run of all the time was
maxtrackever = min(tracks, key = pace )
# gets the sec format for the maxpace
maxpacemin = int(pace(maxtrackever))
maxpacesec = int((pace(maxtrackever) - maxpacemin) *60)
print(' --- The fastest track of all the time was '+str(maxtrackever)+'\n --- your avg pace was '+str(maxpacemin)+':'+str(maxpacesec)+'min/km\n')

## Distribution of where I went to run
import matplotlib.pyplot as plt
piedata = {}
for t in tracks :
    if t.classification in piedata :
        piedata[t.classification] += t.total_meters
    else :
        piedata[t.classification] = t.total_meters
plt.pie(piedata.values(), labels=piedata.keys(), shadow=True, autopct='%1.1f%%')
print(' --- Here is a pie.\n')
plt.show()






# Write down the dictionary of the classification
pickle.dump(tags, open('./TMP/db_tags','w'))
pickle.dump(tracks, open('./TMP/db_tracks','w'))







